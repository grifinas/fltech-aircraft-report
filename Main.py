from models.Aircraft import Aircraft
from views.AircraftTable import AircraftTable
from services.Mailer import Mailer

if __name__ == "__main__":
    # Instantiate aircraft model
    aircraftModel = Aircraft()

    # Get aircrafts
    european = aircraftModel.getEuropeanAircrafts()
    other = aircraftModel.getOtherAircrafts()

    # Instantiate class for view generation
    table = AircraftTable()

    # Send emails
    mailer = Mailer()
    mailer.send("to@example.com", table.display(european), 'european aircraft')
    mailer.send("to@example.com", table.display(other), 'other aircraft')

