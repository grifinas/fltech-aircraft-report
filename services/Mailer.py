import configparser
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class Mailer:
    def __init__(self):
        config = configparser.ConfigParser()
        config.read('config.ini')

        self.server = smtplib.SMTP(config['MAILER']['HOST'], config['MAILER']['PORT'])

        self.server.login(config['MAILER']['USERNAME'], config['MAILER']['PASSWORD'])

        self.fromEmail = config['MAILER']['FROM']
        
    def __del__(self):
        self.server.quit()

    def send(self, to, message, subject):
        """Send a HTML email message"""

        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = self.fromEmail
        msg['To'] = to

        msg.attach(MIMEText(message, 'html'))

        self.server.sendmail(self.fromEmail, to, msg.as_string())

