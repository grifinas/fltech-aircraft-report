## Requires 
* `mysqlclient` for database connection
* python3

## Notes
* No data chunking was implemented, SQL queries load the entire table into memory given a large enough dataset you'll run out of memory
* SQLs in migrations directory asumes that DBName is fltech, take care when running these queries
* Mysqlclient was chosen for a fairly barebones solution, some better tools exist
* Same reason as above no robust html formating solution was chosen `string.format` was deemed enough