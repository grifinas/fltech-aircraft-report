from views.View import View

class AircraftTable(View):
    def display(self, data):
        """Renders and returns html table from given aircraft data"""

        header = self.format('aircraft_header')
        return self.format('table', header = header, body = self.__makeBody(data))

    def __makeBody(self, data):
        """Make the table body using data"""

        total = ''

        for aircraft in data:
            aircraft['ROW_STYLE'] = self.__getAircraftStyle(aircraft)
            total += self.format('aircraft', **aircraft)

        return total

    def __getAircraftStyle(self, aircraft):
        """Apply css rules to aircraft row in table"""

        if aircraft['isEU'] and aircraft['isEuropean']:
            return 'background-color: lightblue'
        elif aircraft['isEuropean']:
            return 'background-color: rgb(255, 150, 150)'

        return ''

