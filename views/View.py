class View:
    def format(self, file, **kwargs):
        """Base view formating, taking a file from html folder and formating via string.format with kwargs"""

        with open('views/html/' + file + '.html', 'r') as f:
            file_contents = f.read()

        return file_contents.format(**kwargs)