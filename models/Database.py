import configparser
import MySQLdb

class Connection:
    def __init__(self):
        config = configparser.ConfigParser()
        config.read('config.ini')

        self.db = MySQLdb.connect(
            host=config['DATABASE']['HOST'],
            user=config['DATABASE']['USERNAME'],
            passwd=config['DATABASE']['PASSWORD'],
            db=config['DATABASE']['NAME']
        )

        self.cursor = self.db.cursor(MySQLdb.cursors.DictCursor)

    def __del__(self):
        self.db.close()

    def execute(self, sql):
        """Execute given sql instructions and return output"""

        self.cursor.execute(sql)

        return self.cursor.fetchall()

        