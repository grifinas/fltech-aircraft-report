from models.Database import Connection

class Aircraft(Connection):
    def getAircraft(self):
        """Get all aircarfts registered in the database"""
        
        return self.execute(self.__getAircraftSQL())

    def getEuropeanAircrafts(self):
        """Get all aircrafts, whose company is registered in Europe"""

        sql = self.__getAircraftSQL() + ' WHERE COUNTRY_CODES.SDF_COC_002 = "Europe"'

        return self.execute(sql)

    def getOtherAircrafts(self):
        """Get all aircrafts, whose company is not in Europe"""

        sql = self.__getAircraftSQL() + ' WHERE COUNTRY_CODES.SDF_COC_002 != "Europe"'

        return self.execute(sql)

    def __getAircraftSQL(self):
        """Get aircraft SQL"""

        return '''
            SELECT 
                TAIL_NUMBER, 
                MODEL_NUMBER, 
                MODEL.description as MODEL_DESCRIPTION, 
                COMPANIES.COMPANY_NAME as OWNER_COMPANY_NAME, 
                COUNTRY_CODES.CODE as COMPANY_COUNTRY_CODE, 
                COUNTRY_CODES.COUNTRY_NAME as COMPANY_COUNTRY_NAME,
                IF(COUNTRY_CODES.SDF_COC_003 = "T", 1, 0) as isEU,
                if(COUNTRY_CODES.SDF_COC_002 = "Europe", 1, 0) as isEuropean
            FROM 
                AIRCRAFT
            JOIN
                COMPANIES ON COMPANIES.CMP_AUTO_KEY = AIRCRAFT.CMP_OWNER
            JOIN
                COUNTRY_CODES ON COUNTRY_CODES.COC_AUTO_KEY = COMPANIES.COC_AUTO_KEY
            JOIN
                MODEL ON MODEL.MDL_AUTO_KEY = AIRCRAFT.MDL_AUTO_KEY
        '''